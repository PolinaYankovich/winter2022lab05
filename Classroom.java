public class Classroom{
	private String subject;
	private int code;
	private Student[] studnets;
	
	public Classroom(String subject, int code, Student[] studnet){
		this.subject=subject;
		this.code=code;
		this.studnets=studnet;
	}
	public String getSubject(){
		return this.subject;
	}
	public int getCode(){
		return this.code;
	}
	public Student[] getStudents(){
		return this.studnets;
	}
	
	public String toString(){
		String studentList="";
		for(int i=0; i<this.studnets.length; i++){
		studentList=studentList+this.studnets[i].toString()+"\n";}
		return studentList;
}
}