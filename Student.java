public class Student{
	private String id;
	private String name;
	private String program;
	
	public String getId(){
		return this.id;
	}
	public String getName(){
		return this.name;
	}
	public String getProgram(){
		return this.program;
	}
	public Student(String id, String name, String program){
		this.id=id;
		this.name=name;
		this.program=program;
	}
	public String toString(){
		return this.name+" : "+this.id+" , "+this.program;
	}
}